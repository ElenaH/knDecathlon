### How to run the project
* import the project as an existing Maven project to Eclipse
  ->File->Import->Maven->Existing Maven projects -> Browse -> knDecathlon/Javacode
* Maven build with goals "clean install exec:java". It will run tests and the program automatically.
  ->Run as->Run configurations -> goals: "clean install exec:java"
  ->Run as-> Maven build
* The output file can be found in Javacode/ouput.xml; it is not indented automatically, open it in Eclipse and press Ctrl+Shift+f to format.
* NB! if you wish to change input file names or input format please change InputFormat.java accordingly.
* Put your custom input files to \knDecathlon\JavaCode\src\main\resources