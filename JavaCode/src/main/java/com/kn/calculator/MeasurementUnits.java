/**
 * An enum with all acceptable measurement units for sporting events names.
 * The calculator events must have one of these measurement units.
 * Otherwise the calculator cannot be created.
 * @author ElenaHeinsalu
 * @since 27.03.2018
 **/

package com.kn.calculator;

public enum MeasurementUnits {
	METRES,
	CENTIMETRES,
	SECONDS,
	MINUTES;
	
	public static MeasurementUnits stringToUnits(String string)throws IllegalArgumentException {
		switch (string.trim().toUpperCase()) {
		case "METRES":
			return MeasurementUnits.METRES;
		case "CENTIMETRES":
			return MeasurementUnits.CENTIMETRES;
		case "SECONDS":
			return MeasurementUnits.SECONDS;
		case "MINUTES":
			return MeasurementUnits.MINUTES;		
		default:
			throw new IllegalArgumentException("No corresponding measurement units found.");
		}
	}
}