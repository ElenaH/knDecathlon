/**
 * An object.
 * Is created by reading input information about ten events.
 * Constructor throws IllegalArgumentException if input is incorrect.
 * The only public method calculates total score for a given athlete results.
 * @author ElenaHeinsalu
 * @since 27.03.2018
 **/

package com.kn.calculator;

import java.util.EnumMap;
import java.util.List;

import static com.kn.inout.InputFormat.*;

public class TotalScoreCalculator {
	protected EnumMap<Event, ScoreCalculator> calculationMap = new EnumMap<>(Event.class);

	public TotalScoreCalculator(List<String[]> input)throws IllegalArgumentException {
		if (input.size() != ATHLETE.size()) {
			throw new IllegalArgumentException(
					"Calculation Map not created! There should be " + ATHLETE.size() + " events.");
		}
		int lineNumber = 0;
		String[] line;
		for (Event name : Event.values()) {
			line = input.get(lineNumber++);
			if ((line.length == EVENT.size()) && (name.equals(line[0]))) {
				calculationMap.put(name, new ScoreCalculator(line));
			} else {
				throw new IllegalArgumentException(
						"Calculation Map not created! Unsupported format for event No " + (lineNumber + 1));
			}
		}
	}

	public long calculateTotalScore(EnumMap<Event, String> performanceResults) {
		Long totalScore = 0L;
		for (Event name : performanceResults.keySet()) {
			String performance = performanceResults.get(name);
			totalScore += calculationMap.get(name).calculateScore(performance);
		}
		return totalScore;
	}

	protected String calculationMapToString() {
		StringBuilder builder = new StringBuilder();
		for (Event name : Event.values()) {
			builder.append("[name=" + name + "; data=" + calculationMap.get(name).toString() + "]\n");
		}
		return builder.toString();
	}
}