/**
 * An enum with all sporting event names. The calculator has to have data for the same names.
 * Otherwise the calculator cannot be created.
 * @author ElenaHeinsalu
 * @since 27.03.2018
 **/

package com.kn.calculator;

public enum Event {
	_100_M,
	LONG_JUMP,
	SHOT_PUT,
	HIGH_JUMP,
	_400_M,
	_110M_HURDLES,
	DISCUS_THROW,
	POLE_VAULT,
	JAVELIN_THROW,
	_1500_M;

	public boolean equals(String string) {
		if (string.trim().toUpperCase().equals(this.toString())) {
			return true;
		} else {
			return false;
		}
	}	
}