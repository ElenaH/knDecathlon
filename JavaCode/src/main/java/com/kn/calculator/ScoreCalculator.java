/**
 * An object, part of calculationMap in TotalScoreCalculator.
 * The constructor assigns the coefficient fields (a, b, c) and measurement units.
 * These numbers are true for a certain event. The event name is key,
 * and ScoreCalculator is value in calculationMap.
 * There is a one-to-one correspondence.
 * The calculation formula is INT(A(P � B)^C)(for results in metres and centimetres)
 * and INT(A(B � P)^C)(for results in seconds and minutes)
 * Has a public toString method that is needed for testing.
 * Only the method calculateScore(performance) is used by another class (TotalScoreCalculator)
 * which calculates the number of points equivalent for a given performance result.
 * Has to be part of full calculation with ten events.
 * @author ElenaHeinsalu
 * @since 27.03.2018
 **/

package com.kn.calculator;

import static com.kn.calculator.MeasurementUnits.*;
import static com.kn.inout.InputFormat.*;
import static com.kn.inout.Util.*;

public class ScoreCalculator {
	private MeasurementUnits units;
	private double a;
	private double b;
	private double c;

	public ScoreCalculator(String... args) throws IllegalArgumentException {
		if (args.length != EVENT.size()) {
			throw new IllegalArgumentException(
					"Failed to create Event Data. " + EVENT.size() + " values expected: name, units, a, b and c.");
		}		
			this.units = stringToUnits(args[1]);
			this.a = Double.parseDouble(args[2]);
			this.b = Double.parseDouble(args[3]);
			this.c = Double.parseDouble(args[4]);
			if ((this.a <= 0) || (this.b <= 0) || (this.c <= 0)) {
				throw new IllegalArgumentException();
			}
	}

	protected long calculateScore(String performance) {
		double difference = parseDifference(performance);
		double score = a * Math.pow(difference, c);
		return (long) Math.floor(score);
	}

	protected double parseDifference(String performance) throws IllegalArgumentException {
		double difference = 0;
		switch (units) {
		case METRES:
			difference = Double.parseDouble(performance) - b;
			break;
		case CENTIMETRES:
			difference = Double.parseDouble(performance) * 100 - b;
			break;
		case SECONDS:
			difference = b - Double.parseDouble(performance);
			break;
		case MINUTES:
			difference = b - toSeconds(performance);
			break;
		}
		if (difference <= 0) {
			throw new IllegalArgumentException(
					"Wrong input: impossible result. The difference with coefficient b must be positive.");
		}
		return difference;
	}

	protected double toSeconds(String performance) throws IllegalArgumentException {
			if (!isInMinutesFormat(performance)){
				throw new IllegalArgumentException("Time must be provided in minutes and must be under 10 minutes. The format is *m.ss.ms*."); 	
			}
			int splitIndex = performance.indexOf(".");
			double minutes = Double.parseDouble(performance.substring(0, splitIndex));
			double seconds = Double.parseDouble(performance.substring(splitIndex + 1));
			return minutes * 60 + seconds;
	}
	
@Override
	public String toString() {
		return "[units = " + units + ", a = " + a + ", b = " + b + ", c = " + c + "]";	
	}
}