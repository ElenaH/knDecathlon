package com.kn;

import org.apache.log4j.Logger;

import com.kn.athlete.AthleteRanker;
import com.kn.calculator.TotalScoreCalculator;

import static com.kn.inout.InputFormat.*;
import static com.kn.inout.Util.*;

public class Main {
	private final static Logger LOGGER = Logger.getLogger(Main.class);

	public static void main(String[] args) {
		try {
			setSdoutToFile("output.xml");
			LOGGER.info("Successfully redirected output to 'output.xml'.");
			TotalScoreCalculator calculator = new TotalScoreCalculator(
					readSdinFromFile(EVENT.file(), EVENT.delimiter()));
			LOGGER.info("Successfully read calculation map.");
			AthleteRanker ranker = new AthleteRanker(readSdinFromFile(ATHLETE.file(),
					ATHLETE.delimiter()), calculator);
			LOGGER.info("Successfully created athlete list and calculated scores.");
			System.out.println(ranker.toXML());			
			LOGGER.info("Successfully created 'output.xml'.");
		} catch (Exception e) {
			LOGGER.error("Calculation was aborted. " + e);
		}
	}
}