/**
 * A data structure class.
 * It can be created only if name and performance results are provided,
 * as well as the calculator  to use.
 * Calculator provides coefficients for every result in every sporting event.
 * In case calculator and athlete information don't match, the constructor
 * throws IllegalArgumentException.
 * The only field that can be injected through a setter is a relative place
 * that depends on other athletes in a list.
 * @author ElenaHeinsalu
 * @since 27.03.2018
 **/

package com.kn.athlete;

import java.util.EnumMap;

import com.kn.calculator.Event;
import com.kn.calculator.TotalScoreCalculator;

import static com.kn.inout.InputFormat.*;
import static com.kn.inout.Util.*;

public class Athlete {
	private String athleteName;
	private EnumMap<Event, String> performanceResults = new EnumMap<>(Event.class);
	private Long totalScore;
	private String place;

	public Athlete(TotalScoreCalculator calculator, String... args) throws IllegalArgumentException{
		if (args.length != ATHLETE.size() + 1) {
			throw new IllegalArgumentException(
					"Failed to create Athlete. Name and " + ATHLETE.size() + " performance results expected.");
		}
		int index = 0;
		this.athleteName = args[index];
		for (Event name : Event.values()) {
			performanceResults.put(name, args[++index]);
		}
		this.totalScore = calculator.calculateTotalScore(performanceResults);
		this.place = "unranked";
	}

	public Long getTotalScore() {
		return totalScore;
	}

	public String toXML() {
		StringBuilder athleteData = new StringBuilder();
		athleteData.append(makeXmlNode("name", athleteName));
		athleteData.append(makeXmlNode("place", place));
		athleteData.append(makeXmlNode("total", totalScore.toString()));
		StringBuilder performanceNodes = new StringBuilder();
		for (Event name : Event.values()) {
			performanceNodes.append(makeXmlNode(name.toString().toLowerCase(), performanceResults.get(name)));
		}
		athleteData.append(makeXmlNode("performance", performanceNodes.toString()));
		return makeXmlNode(this.getClass().getSimpleName().toLowerCase(), athleteData.toString());
	}

	public String getPlace() {
		return place;
	}

	protected void setPlace(String place) {
		this.place = place;
	}
}