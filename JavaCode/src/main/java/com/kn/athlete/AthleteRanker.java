/**
 * An object.
 * The constructor creates a list of athletes, sorts them by score and they are ranked accordingly.
 * Then their places are assigned.
 * It does not accept changes in the list after the ranking has taken place.
 * The only available public method creates an output XML String with the ranking that was created.
 * In order to rank another list, a new AthleteRanker object must be created.
 * @author ElenaHeinsalu
 * @since 27.03.2018
 **/

package com.kn.athlete;

import java.util.*;

import com.kn.calculator.TotalScoreCalculator;

import static com.kn.inout.Util.*;

public class AthleteRanker {
	List<Athlete> allAthletes;
	TreeMap<Long, String> scoreRank;

	public AthleteRanker(List<String[]> input, TotalScoreCalculator calculator) {
		allAthletes = createAndSortAllAthletes(input, calculator);
		if (allAthletes.size() != 0) {
			scoreRank = createScoreRank();
			allAthletes = assignPlacesByScoreRank();
		}
	}

	private List<Athlete> createAndSortAllAthletes(List<String[]> input, TotalScoreCalculator calculator) {
		List<Athlete> allAthletes = new ArrayList<Athlete>();
		for (String[] athleteData : input) {
			allAthletes.add(new Athlete(calculator, athleteData));
		}
		Collections.sort(allAthletes, (new Comparator<Athlete>() {
			@Override
			public int compare(Athlete athlete1, Athlete athlete2) {
				return athlete2.getTotalScore().compareTo(athlete1.getTotalScore());
			}}));
		return allAthletes;
	}

	private TreeMap<Long, String> createScoreRank() {
		TreeMap<Long, String> scoreRank = new TreeMap<Long, String>();
		int size = allAthletes.size();
		Long score = getScore(0,size);
		Long nextScore;		
		Integer minPlace = 1;
		Integer maxPlace = 1;
		
		for (int index = 1; index <= size; index++) {
			nextScore = getScore(index, size);
			if (nextScore < score) {
				scoreRank.put(score, toRangeString(minPlace, maxPlace));
				score = nextScore;
				minPlace = ++maxPlace;
			} else {
				++maxPlace;
			}
		}
		return scoreRank;
	}
	
	private Long getScore(int index, int maxIndex){
		if (index == maxIndex) {
			return 0L;
		} else {
			return allAthletes.get(index).getTotalScore();
		}
	}

	private List<Athlete> assignPlacesByScoreRank() {
		for (Athlete athlete : allAthletes) {
			athlete.setPlace(scoreRank.get(athlete.getTotalScore()));
		}
		return allAthletes;
	}

	public String toXML() {
		StringBuilder builder = new StringBuilder();
		for (Athlete athlete : allAthletes) {
			builder.append(athlete.toXML());
		}
		return ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + makeXmlNode("athletes", builder.toString()));
	}
}