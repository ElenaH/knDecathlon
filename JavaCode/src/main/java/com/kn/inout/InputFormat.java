/**
 * Utility class.
 * You can change the file names here and the delimiter values for reading from files.
 * ATHLETE.size(): The number of 10 events in Decathlon cannot be changed!
 * EVENT.size(): Event size "5" cannot be changed!
 * The calculations strictly require: eventName, units, a, b, c * 
 * @author ElenaHeinsalu
 * @since 27.03.2018
 **/

package com.kn.inout;

public enum InputFormat {
	ATHLETE(10, ";", "athletes.csv"),
	EVENT( 5, ";","events.csv");
	private final int SIZE;
	private String delimiter;	
	private String file;
 	
	private InputFormat(int SIZE, String delimiter, String file) {
		this.SIZE = SIZE;
		this.delimiter = delimiter;		
		this.file = file;
	}
	
	public int size() {
		return this.SIZE;
	}

	public String delimiter() {
		return this.delimiter;
	}

	public String file() {
		return this.file;
	}
}