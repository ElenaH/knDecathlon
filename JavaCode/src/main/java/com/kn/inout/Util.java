/**
 * Utility class.
 * makeXmlNode(tag,node) creates a node <tag>node</tag>
 * isInMinutesFormat(minutes) checks if the minutes String in the correct format and can be parsed
 * toRangeString(min, max) creates a string that represents range, e.g. "3-5" for 3-5, "7" for (7,7)
 * setSdoutToFile(outFile) redirects standard output to a file
 * readSdinFromFile (fileName, fieldDelimeter) reads information from a file as standard input
 * @author ElenaHeinsalu
 * @since 27.03.2018
 **/

package com.kn.inout;

import java.io.*;
import java.util.*;

import com.kn.Main;

public final class Util {
	
	private Util(){}
	
	public static String makeXmlNode(String tag, String node) {
		return "\n<" + tag + ">" + node + "</" + tag + ">";
	}

	public static boolean isInMinutesFormat(String minutes) {
		if (minutes.matches("[0-9]{1}\\.[0-9]{2}\\.[0-9]{2}")){
			return true;
		}
		else{
			return false;
		}
	}	
	
	public static String toRangeString(Integer min, Integer max) {		
		if (min != max) {
			return min.toString().concat("-").concat(max.toString());
		} else {
			return min.toString();
		}
	}

	public static void setSdoutToFile(String outFile) throws FileNotFoundException  {
		System.setOut(new PrintStream(new FileOutputStream(outFile, false)));
	}

	public static List<String[]> readSdinFromFile(String fileName, String fieldDelimeter) {
		List<String[]> values = new ArrayList<String[]>();
		System.setIn(Main.class.getResourceAsStream("/" + fileName));
		Scanner scanner = new Scanner(System.in);
		while (scanner.hasNext()) {
			values.add(scanner.nextLine().split(fieldDelimeter));
		}
		scanner.close();
		return values;
	}
}