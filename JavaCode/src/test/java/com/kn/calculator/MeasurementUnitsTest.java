package com.kn.calculator;

import static com.kn.calculator.MeasurementUnits.*;

import static org.junit.Assert.*;
import org.junit.Test;

public class MeasurementUnitsTest {

	@Test
	public void testStringToUnit_correctUnits() {
		assertEquals(METRES,stringToUnits(" metres"));
		assertEquals(METRES,stringToUnits("Metres "));
		assertEquals(METRES,stringToUnits("METRES"));
		assertEquals(SECONDS,stringToUnits("seconds"));
		assertEquals(MINUTES,stringToUnits("minutes"));
		assertEquals(CENTIMETRES,stringToUnits("centimetres"));		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testStringToUnit_unknownUnits() {
	    stringToUnits("hours");
	}
}