package com.kn.calculator;

import static org.junit.Assert.*;
import org.junit.Test;

import com.kn.calculator.Event;

public class EventTest {

	@Test
	public void testEquals_correctEventNames() {
		assertTrue(Event._100_M.equals("_100_M"));
		assertTrue(Event.LONG_JUMP.equals(" LONG_JUMP"));
		assertTrue(Event.SHOT_PUT.equals("shot_PUT"));
		assertTrue(Event.HIGH_JUMP.equals("high_jump"));
		assertTrue(Event._400_M.equals("_400_m "));
		assertTrue(Event._110M_HURDLES.equals(" _110M_HURDLES "));
		assertTrue(Event.DISCUS_THROW.equals("DISCUS_THROW"));
		assertTrue(Event.POLE_VAULT.equals("POLE_VAULT"));
		assertTrue(Event._1500_M.equals("_1500_M        "));
	}
		
	@Test
	public void testEquals_unknownEventName() {
		assertFalse(Event._100_M.equals("_200_m"));		
	}
}