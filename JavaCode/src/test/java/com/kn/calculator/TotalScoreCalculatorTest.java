package com.kn.calculator;

import static com.kn.UtilForTesting.*;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import com.kn.calculator.TotalScoreCalculator;
public class TotalScoreCalculatorTest {

	private static List<String[]> events = new ArrayList<String[]>();

	@Before
	public void setUp() {
		events = makeTenEvents();
	}

	@Test
	public void testTotalScoreCalculator_validMap() {
		TotalScoreCalculator map = new TotalScoreCalculator(events);
		assertEquals(eventsListToString(events), map.calculationMapToString());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testTotalScoreCalculator_invalidMapWrongEventsNumber() {
		events.remove(9);
		new TotalScoreCalculator(events);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testTotalScoreCalculator_invalidMapWrongOrder() {
		String[] firstEvent = events.get(0);
		events.remove(0);
		events.add(firstEvent);
		new TotalScoreCalculator(events);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testTotalScoreCalculator_invalidMapWrongSizeAndOrder() {
		events.remove(0);
		String string = "_100_M;SECONDS;25.4347;18.0";
		String[] event = string.split(";");
		events.add(event);
		new TotalScoreCalculator(events);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testTotalScoreCalculator_invalidMapWrongSize() {
		events.remove(9);
		String string = "_1500_M;MINUTES;0.03768;480.0";
		String[] event = string.split(";");
		events.add(event);
		new TotalScoreCalculator(events);
	}

	@Test
	public void testCalculateTotalScore() {
		TotalScoreCalculator map = new TotalScoreCalculator(events);
		assertEquals(map.calculateTotalScore(createTestPerformanceResults()), 9045L);
	}
}