package com.kn.calculator;

import static org.junit.Assert.*;
import org.junit.Test;

import com.kn.calculator.ScoreCalculator;

public class ScoreCalculatorTest {

	@Test(expected =  IllegalArgumentException.class)
	public void testScoreCalculator_wrongNumberOfArguments() {
		new ScoreCalculator("100m","minutes"); 
	}
	
	@Test(expected =  IllegalArgumentException.class)
	public void testScoreCalculator_nonExistantUnits() {
		new ScoreCalculator("_100_M", "pascals", "1", "2","2"); 
	}
	
	@Test(expected =  IllegalArgumentException.class)
	public void testScoreCalculator_nonNumberArguments() {
		new ScoreCalculator("_100_M", "MINUTES", "3.4.3", "a","7="); 
	}
	
	@Test(expected =  IllegalArgumentException.class)
	public void testScoreCalculator_negativeArgumentA() {
		new ScoreCalculator("_100_M", "MINUTES", "-1", "2","2"); 	
	}
	
	@Test(expected =  IllegalArgumentException.class)
	public void testScoreCalculator_negativeArgumentB() {
		new ScoreCalculator("_100_M", "MINUTES", "1", "-2","2");
	}
	
	@Test(expected =  IllegalArgumentException.class)
	public void testScoreCalculator_negativeArgumentC() {		
		new ScoreCalculator("_100_M", "MINUTES", "1", "2","-2"); 
	}
	
	@Test
	public void testToSeconds(){
		ScoreCalculator event= new ScoreCalculator("1500_M","MINUTES","0.03768","480","1.85");
		double time=event.toSeconds("4.23.33");
		assertTrue(time==4*60+23.33);		
	}
	
	@Test (expected =  IllegalArgumentException.class)
	public void testToSeconds_illegalArgument(){
		ScoreCalculator event= new ScoreCalculator("1500_M","MINUTES","0.03768","480","1.85");
		event.toSeconds(".4.2");			
	}
	
	@Test
	public void testToString() {
		ScoreCalculator event=new ScoreCalculator("1500_M","MINUTES","0.03768","480","1.85");
		assertEquals("[units = MINUTES, a = 0.03768, b = 480.0, c = 1.85]", event.toString());
	}

	@Test (expected =  IllegalArgumentException.class)
	public void testParseDifference_incorrectUnits() {
	ScoreCalculator event=new ScoreCalculator("1500_M","Hours","0.03768","480","1.85");
		event.parseDifference("4.17.52");	
	}
	
	@Test
	public void testParseDifference_correctInputMetres() {
	ScoreCalculator event=new ScoreCalculator("SHOT_PUT","METRES","51.39","1.5","1.05");
		assertTrue(event.parseDifference("14.52")==14.52-1.5);		
	}	

	@Test
	public void testParseDifference_correctInputCentimetres() {
	ScoreCalculator event=new ScoreCalculator("LONG_JUMP","CENTIMETRES","0.14354","220","1.4");
		assertTrue(event.parseDifference("7.88")==7.88*100-220.0);		
	}
	
	@Test
	public void testParseDifference_correctInputMinutes() {
	ScoreCalculator event=new ScoreCalculator("1500_M","MINUTES","0.03768","480","1.85");
		assertTrue(event.parseDifference("4.17.52")==480-event.toSeconds("4.17.52"));		
	}
	
	@Test
	public void testParseDifference_correctInputSeconds() {
	ScoreCalculator event=new ScoreCalculator("_100_M","SECONDS","25.4347","18","1.81");
		assertTrue(event.parseDifference("10.23")==18-10.23);		
	}
	
	@Test  (expected =  IllegalArgumentException.class)
	public void testParseDifference_incorrectInput_differenceNegative() {
	ScoreCalculator event=new ScoreCalculator("_100_M","SECONDS","25.4347","18","1.81");
		event.parseDifference("20");		
	}

	@Test  (expected =  IllegalArgumentException.class)
	public void testParseDifference_incorrectInput_differenceZero() {
	ScoreCalculator event=new ScoreCalculator("_100_M","SECONDS","25.4347","18","1.81");
		event.parseDifference("18");		
	}

	@Test
	public void testCalculateScore_testWithCalculation() {
		ScoreCalculator event=new ScoreCalculator("_100_M","SECONDS","25.4347","18","1.81");
		double power=Math.pow((18-10.21),1.81);
		long expected=(long)Math.floor(25.4347*power);
		assertTrue( event.calculateScore("10.21")==expected );
	}

	@Test
	public void testCalculateScore_testWithSearchableResults() {
		//100m, 10.21 sec, best ever in a decathlon, 1,044 points.
		//https://www.iaaf.org/news/report/9039-points-eaton-breaks-world-record-before
		ScoreCalculator event=new ScoreCalculator("_100_M","SECONDS","25.4347","18","1.81");
		long expected=1044;
		assertTrue( event.calculateScore("10.21")==expected );
	}
}