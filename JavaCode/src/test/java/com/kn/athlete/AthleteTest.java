package com.kn.athlete;

import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;

import com.kn.calculator.TotalScoreCalculator;

import java.util.ArrayList;
import java.util.List;

import static com.kn.UtilForTesting.*;

public class AthleteTest {

	private static List<String[]> events = new ArrayList<String[]>();
	private static TotalScoreCalculator map;
	private static Athlete athlete;
	private static String place = "2-3";

	@BeforeClass
	public static void setUp() {
		events = makeTenEvents();
		map = new TotalScoreCalculator(events);
		athlete = new Athlete(map, "Ashton Eaton", "10.23", "7.88", "14.52", "2.01", "45.00", "13.69", "43.34", "5.20",
				"63.63", "4.17.52");
		athlete.setPlace(place);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAthlete_wrongNumberOfArguments() {
		athlete = new Athlete(map, "Ashton Eaton", "10.23", "7.88", "14.52", "2.01", "45.00", "13.69", "43.34", "5.20",
				"63.63");
	}

	@Test
	public void testAthleteGetTotalScore() {
		assertEquals(athlete.getTotalScore(), (Long) 9045L);
	}

	@Test
	public void testToXML() {
		String expected = "\n<athlete>\n<name>Ashton Eaton</name>"
	                     + "\n<place>2-3</place>"
				          + "\n<total>9045</total>"
				           + "\n<performance>\n<_100_m>10.23</_100_m>"
				            + "\n<long_jump>7.88</long_jump>"
				             + "\n<shot_put>14.52</shot_put>"
				              + "\n<high_jump>2.01</high_jump>"
				               + "\n<_400_m>45.00</_400_m>"
				                + "\n<_110m_hurdles>13.69</_110m_hurdles>"
				                 + "\n<discus_throw>43.34</discus_throw>"
				                  + "\n<pole_vault>5.20</pole_vault>"
				                   + "\n<javelin_throw>63.63</javelin_throw>"
				                    + "\n<_1500_m>4.17.52</_1500_m></performance></athlete>";
		assertEquals(expected,athlete.toXML());
	}

	@Test
	public void testGetSetPlace() {
		assertEquals(athlete.getPlace(), place);

	}
}