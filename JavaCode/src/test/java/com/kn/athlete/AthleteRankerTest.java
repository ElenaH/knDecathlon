package com.kn.athlete;

import static com.kn.UtilForTesting.makeTenEvents;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import com.kn.calculator.TotalScoreCalculator;

public class AthleteRankerTest {	
	
	private static List<String[]> events = new ArrayList<String[]>();
	private static TotalScoreCalculator map;

	@BeforeClass
	public static void setUp() {
		events = makeTenEvents();
		map = new TotalScoreCalculator(events);		
	}
	
	@Test
	public void testAthleteRankerAndXMLOutput() {
		List <String[]> athletes =  new ArrayList<String[]>();
		String[] athleteString1="Ashton Eaton;10.23;7.88;14.52;2.01;45.00;13.69;43.34;5.20;63.63;4.17.52".split(";");
		athletes.add(athleteString1);
		athletes.add(athleteString1); //adding the same results assigns places 1-2
		String[] athleteString2="John Smith;12.61;5.00;9.22;1.50;60.39;16.43;21.60;2.60;35.81;5.25.72".split(";");
		athletes.add(athleteString2);
		AthleteRanker athleteRanker= new AthleteRanker(athletes, map);		
		String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
			    +"\n<athletes>\n<athlete>\n<name>Ashton Eaton</name>"
                 + "\n<place>1-2</place>"
		          + "\n<total>9045</total>"
		           + "\n<performance>\n<_100_m>10.23</_100_m>"
		            + "\n<long_jump>7.88</long_jump>"
		             + "\n<shot_put>14.52</shot_put>"
		              + "\n<high_jump>2.01</high_jump>"
		               + "\n<_400_m>45.00</_400_m>"
		                + "\n<_110m_hurdles>13.69</_110m_hurdles>"
		                 + "\n<discus_throw>43.34</discus_throw>"
		                  + "\n<pole_vault>5.20</pole_vault>"
		                   + "\n<javelin_throw>63.63</javelin_throw>"
		                    + "\n<_1500_m>4.17.52</_1500_m></performance></athlete>"
		                     +"\n<athlete>\n<name>Ashton Eaton</name>"
		                      + "\n<place>1-2</place>"
		    		           + "\n<total>9045</total>"
		    		            + "\n<performance>\n<_100_m>10.23</_100_m>"
		    		             + "\n<long_jump>7.88</long_jump>"
		    		              + "\n<shot_put>14.52</shot_put>"
		    		               + "\n<high_jump>2.01</high_jump>"
		    		                + "\n<_400_m>45.00</_400_m>"
		    		                 + "\n<_110m_hurdles>13.69</_110m_hurdles>"
		    		                  + "\n<discus_throw>43.34</discus_throw>"
		    		                   + "\n<pole_vault>5.20</pole_vault>"
		    		                    + "\n<javelin_throw>63.63</javelin_throw>"
		    		                     + "\n<_1500_m>4.17.52</_1500_m></performance></athlete>"
		    		                      + "\n<athlete>\n<name>John Smith</name>"
		    		                       + "\n<place>3</place>"
		    		                        + "\n<total>4200</total>"
		    		                         + "\n<performance>\n<_100_m>12.61</_100_m>"
		    		                          + "\n<long_jump>5.00</long_jump>"
		    		                           + "\n<shot_put>9.22</shot_put>"
		    		                            + "\n<high_jump>1.50</high_jump>"
		    		                             + "\n<_400_m>60.39</_400_m>"
		    		                              + "\n<_110m_hurdles>16.43</_110m_hurdles>"
		    		                               + "\n<discus_throw>21.60</discus_throw>"
		    		                                + "\n<pole_vault>2.60</pole_vault>"
		    		                                 + "\n<javelin_throw>35.81</javelin_throw>"
		    		                                  + "\n<_1500_m>5.25.72</_1500_m></performance></athlete>"
		    		                                   + "</athletes>";
		assertEquals(expected,athleteRanker.toXML());
	}	
}