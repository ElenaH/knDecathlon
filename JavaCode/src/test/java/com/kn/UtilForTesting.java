package com.kn;

import static com.kn.calculator.Event.*;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;

import com.kn.calculator.Event;

public class UtilForTesting {

	public static List<String[]> makeTenEvents() {
		List<String[]> eventsInput = new ArrayList<String[]>();

		String string = "_100_M;SECONDS;25.4347;18.0;1.81";
		String[] event = string.split(";");
		eventsInput.add(event);

		string = "LONG_JUMP;CENTIMETRES;0.14354;220.0;1.4";
		event = string.split(";");
		eventsInput.add(event);

		string = "SHOT_PUT;METRES;51.39;1.5;1.05";
		event = string.split(";");
		eventsInput.add(event);

		string = "HIGH_JUMP;CENTIMETRES;0.8465;75.0;1.42";
		event = string.split(";");
		eventsInput.add(event);

		string = "_400_M;SECONDS;1.53775;82.0;1.81";
		event = string.split(";");
		eventsInput.add(event);

		string = "_110M_HURDLES;SECONDS;5.74352;28.5;1.92";
		event = string.split(";");
		eventsInput.add(event);

		string = "DISCUS_THROW;METRES;12.91;4.0;1.1";
		event = string.split(";");
		eventsInput.add(event);

		string = "POLE_VAULT;CENTIMETRES;0.2797;100.0;1.35";
		event = string.split(";");
		eventsInput.add(event);

		string = "JAVELIN_THROW;METRES;10.14;7.0;1.08";
		event = string.split(";");
		eventsInput.add(event);

		string = "_1500_M;MINUTES;0.03768;480.0;1.85";
		event = string.split(";");
		eventsInput.add(event);
		return eventsInput;
	}

	public static String eventsListToString(List<String[]> tenEvents) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < 10; i++) {
			builder.append("[name=" + tenEvents.get(i)[0] + "; data=[units = " + tenEvents.get(i)[1] + ", a = "
					+ tenEvents.get(i)[2] + ", b = " + tenEvents.get(i)[3] + ", c = " + tenEvents.get(i)[4] + "]]\n");
		}
		return builder.toString();
	}

	public static EnumMap<Event, String> createTestPerformanceResults() {
		// Ashton
		// Eaton;10.23;7.88;14.52;2.01;45.00;13.69;43.34;5.20;63.63;4.17.52
		// Total: 9045
		EnumMap<Event, String> results = new EnumMap<>(Event.class);
		results.put(_100_M, "10.23");
		results.put(LONG_JUMP, "7.88");
		results.put(SHOT_PUT, "14.52");
		results.put(HIGH_JUMP, "2.01");
		results.put(_400_M, "45.00");
		results.put(_110M_HURDLES, "13.69");
		results.put(DISCUS_THROW, "43.34");
		results.put(POLE_VAULT, "5.20");
		results.put(JAVELIN_THROW, "63.63");
		results.put(_1500_M, "4.17.52");
		return results;
	}
}