package com.kn.inout;

import static org.junit.Assert.*;
import static com.kn.inout.InputFormat.*;
import org.junit.Test;

public class InputFormatTest {

	@Test
	public void testDelimeter() {
	assertEquals(ATHLETE.delimiter(), ";");
	assertEquals(EVENT.delimiter(), ";");
	}

	@Test
	public void testSize() {
		assertEquals(ATHLETE.size(), 10);
		assertEquals(EVENT.size(), 5);
	}

	@Test
	public void testFile() {
		assertEquals(ATHLETE.file(), "athletes.csv");
		assertEquals(EVENT.file(), "events.csv");
	}
}