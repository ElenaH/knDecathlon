package com.kn.inout;

import static com.kn.inout.Util.*;
import static org.junit.Assert.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class UtilTest {

	@Test
	public void testmakeXMLnode() {
		assertEquals(makeXmlNode("tag", "node"), "\n<tag>node</tag>");
	}
@Test
	public void testIsInMinutesFormat(){
	assertTrue(isInMinutesFormat("2.34.00"));
	assertFalse(isInMinutesFormat("02.34.00"));
	assertFalse(isInMinutesFormat("2.34."));
}
	
	
	@Test
	public void testToRangeString_differentValues() {
		assertEquals(toRangeString(1, 4), "1-4");
	}

	@Test
	public void testToRangeString_sameValue() {
		assertEquals(toRangeString(4, 4), "4");
	}

	@Test
	public void testSetSdoutToFile_fileCanBeCreated() throws Exception {
		String fileName = "test_output.txt";
		String actualString = "";
		String expectedString = "This is file content.";
		setSdoutToFile(fileName);
		System.out.println(expectedString);
		String line = "";
		try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
			while ((line = bufferedReader.readLine()) != null) {
				actualString = actualString.concat(line);
			}
		} catch (IOException e) {
			throw e;
		}
		assertEquals(expectedString, actualString);

	}

	@Test
	public void testReadSdinFromFile() {
		// file contents
		// 0;1;2
		// 0;1;2
		// The test checks if two String arrays are being added
		// They are the same to simplify the comparison
		// The test checks if several lines can be read and if delimeter works
		// The order of the list elements does not matter for this test case
		String expElement;
		String actElement;
		int lineLength = 3;
		String delimeter = ";";
		String fileName = "test_input.txt";
		List<String[]> actual = readSdinFromFile(fileName, delimeter);
		List<String[]> expected = new ArrayList<String[]>();
		Integer element;
		String[] expectedLine = new String[lineLength];
		for (int lines = 0; lines < 2; lines++) {
			for (int symbols = 0; symbols < lineLength; symbols++) {
				element = symbols;
				expectedLine[symbols] = element.toString();
			}
			expected.add(expectedLine);
		}
		for (int lines = 0; lines < 2; lines++) {
			for (int symbols = 0; symbols < lineLength; symbols++) {
				expElement = expected.get(lines)[symbols];
				actElement = actual.get(lines)[symbols];
				assertEquals(expElement, actElement);
			}
		}
	}
}