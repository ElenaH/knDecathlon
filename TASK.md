#### The task is about Decathlon competition.

The input of the Java program is a `CSV` file (see the attachment, please do not try to open it with Excel, use some simple text editor). The program should output an `XML` (to standard output) with all athletes in ascending order of their places, containing all the input data plus total score and the place in the competition (in case of equal scores, athletes must share the places, e.g. 3-4 and 3-4 instead of 3 and 4). 
Formulas for points counting can be found [here] (https://en.wikipedia.org/wiki/Decathlon#Points_system).
* JDK 1.8 should be used
* No external libraries are allowed in addition to the `Java standard API` except `JUnit`.
* Your project should be build using `Maven`

#### And some points we consider very important!

* <i>Code design should be simple yet flexible allowing easy modifications (e.g. new input/output formats)</i>
* <i>Java is object-oriented programming language – use it!</i>
* <i>Clean code principles must be followed!</i>
* <i>Code must be covered with unit-tests!</i>


If you have ANY questions, please do not hesitate to contact us (as we consider communication to be a very important part in software development).
We recommend using the IntelliJ IDEA or Eclipse as the Java IDE.
Please zip your code together with the project file. Keep in mind that we are going to run both your program and the tests.
There is no strict time limit, but try to finish the assignment in a reasonable amount of time.
Good luck!
